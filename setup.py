from setuptools import setup, find_packages

setup(
    name='tal_py_pg_todo',
    version='1.0',
    packages=find_packages(),
    include_package_data=True,
    package_data={'tal_py_pg_todo': ['templates/index.html']},
    install_requires=[
      'flask',
      'flask_sqlalchemy',
      'psycopg2-binary',
    ],
    author='talk',
    author_email='talk474747@gmail.com',
    description='tals package for hafifa',
    entry_points={
        "console_scripts": [
            'talserver = tal_py_pg_todo.app:main',  
        ],
    },
)
