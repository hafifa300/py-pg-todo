import pytest
from flask import url_for
from app import app, db, Todo
import os

@pytest.fixture
def client():
    app.config['TESTING'] = True
    db_uri = f"postgresql://{os.environ['POSTGRES_USER']}:{os.environ['POSTGRES_PASSWORD']}@{os.environ['POSTGRES_SERVICE']}/{os.environ['POSTGRES_DB']}"
    app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
    with app.test_client() as client:
        with app.app_context():  # Enter application context
            db.create_all()
        yield client
        # Cleanup after the tests have completed
        with app.app_context():
            Todo.query.delete()
            db.session.commit()

def test_index(client):
    with app.app_context():  # Enter application context
        response = client.get('/')
        assert response.status_code == 200

def test_add(client):
    with app.app_context():  # Enter application context
        response = client.post('/add', data={'title': 'Test Todo'})
        assert response.status_code == 302  # Redirects to index
        todos = Todo.query.all()
        assert len(todos) == 1
        assert todos[0].title == 'Test Todo'
        assert not todos[0].complete

def test_update(client):
    with app.app_context():  # Enter application context
        todo = Todo(title='Test Todo', complete=False)
        db.session.add(todo)
        db.session.commit()

        response = client.get(f'/update/{todo.id}')
        assert response.status_code == 302  # Redirects to index

        updated_todo = Todo.query.get(todo.id)
        assert updated_todo.complete

def test_delete(client):
    with app.app_context():  # Enter application context
        todo = Todo(title='Test Todo', complete=False)
        db.session.add(todo)
        db.session.commit()

        response = client.get(f'/delete/{todo.id}')
        assert response.status_code == 302  # Redirects to index

        deleted_todo = Todo.query.get(todo.id)
        assert deleted_todo is None

