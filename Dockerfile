FROM python:3.9.19

RUN python -m venv .venv

RUN . .venv/bin/activate

COPY ./dist/tal_py_pg_todo-1.0-py3-none-any.whl .

RUN pip install ./tal_py_pg_todo-1.0-py3-none-any.whl

ENTRYPOINT talserver
