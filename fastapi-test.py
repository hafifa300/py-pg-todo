from fastapi.testclient import TestClient
from app import app, Todo, db

# Create a Flask test client
client = app.test_client()


def test_index():
    # Test GET request to '/'
    response = client.get('/')
    assert response.status_code == 200


def test_add():
    # Test POST request to '/add'
    title = "Test Todo"

    with app.app_context():
        response = client.post('/add', data={'title': title})
        assert response.status_code == 302  # Redirects to index

        # Check if the new todo was added to the database
        todo = Todo.query.filter_by(title=title).first()
        assert todo is not None
        assert todo.title == title
        assert not todo.complete


def test_update():
    with app.app_context():
        # Create a test todo
        todo = Todo(title='Test Todo', complete=False)
        db.session.add(todo)
        db.session.commit()

        # Test GET request to '/update/<int:todo_id>'
        response = client.get(f'/update/{todo.id}')
        assert response.status_code == 302  # Redirects to index

        # Check if the todo was updated in the database
        updated_todo = Todo.query.get(todo.id)
        assert updated_todo.complete


def test_delete():
    with app.app_context():
        # Create a test todo
        todo = Todo(title='Test Todo', complete=False)
        db.session.add(todo)
        db.session.commit()


        # Test GET request to '/delete/<int:todo_id>'
        response = client.get(f'/delete/{todo.id}')
        assert response.status_code == 302  # Redirects to index

        # Check if the todo was deleted from the database
        deleted_todo = Todo.query.get(todo.id)
        assert deleted_todo is None

